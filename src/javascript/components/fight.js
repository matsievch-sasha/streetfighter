import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let leftFighterIndicator = document.getElementById('left-fighter-indicator');
    let rightFighterIndicator = document.getElementById('right-fighter-indicator');

    let healthFirst = {
      full: secondFighter.health,
      current: secondFighter.health,
    }

    let healthSecond = {
      full: firstFighter.health,
      current: firstFighter.health,
    }

    let damage = 0;
    let attacker;
    let defender;
    let healthDefender;
    let fighterIndicator;
    let playerOneCriticalHitCombination = false;
    let playerTwoCriticalHitCombination = false;
    let timeCriticalAttackOne;
    let timeCriticalAttackTwo;
    let intervalCriticalAttack = 10000;

    let pressedFighter = new Set();

    document.addEventListener('keydown', function (event) {
      pressedFighter.add(event.code);
      console.log(pressedFighter);
      playerOneCriticalHitCombination = criticalHitCombination(controls.PlayerOneCriticalHitCombination, pressedFighter);
      playerTwoCriticalHitCombination = criticalHitCombination(controls.PlayerTwoCriticalHitCombination, pressedFighter);

      if (event.code === controls.PlayerOneAttack || event.code === controls.PlayerTwoAttack || playerOneCriticalHitCombination || playerTwoCriticalHitCombination) {
        if (event.code === controls.PlayerOneAttack || event.code === controls.PlayerTwoAttack) {
          //firstFighter attack KeyA
          if (event.code === controls.PlayerOneAttack) {
            if (pressedFighter.has(controls.PlayerOneBlock)) {
              return
            }
            attacker = firstFighter;
            healthDefender = healthSecond;
            fighterIndicator = rightFighterIndicator;
            // if fighter2 has block
            if (pressedFighter.has(controls.PlayerTwoBlock)) {
              defender = secondFighter;
            } else {
              defender = false;
            }
          }
          //secondFighter attack KeyJ
          else if (event.code === controls.PlayerTwoAttack) {
            if (pressedFighter.has(controls.PlayerTwoBlock)) {
              return
            }
            attacker = secondFighter;
            healthDefender = healthFirst;
            fighterIndicator = leftFighterIndicator;
            // if fighter1 has block
            if (pressedFighter.has(controls.PlayerOneBlock)) {
              defender = firstFighter;
            } else {
              defender = false;
            }
          }
          damage = getDamage(attacker, defender)
        }
        else if (playerOneCriticalHitCombination || playerTwoCriticalHitCombination) {
          if (playerOneCriticalHitCombination) {
            if (pressedFighter.has(controls.PlayerOneBlock)) {
              return
            }
            if ((timeCriticalAttackOne === undefined) || ((Date.now() - timeCriticalAttackOne) >= intervalCriticalAttack)) {
              attacker = firstFighter;
              let { attack } = firstFighter;
              healthDefender = healthSecond;
              fighterIndicator = rightFighterIndicator;
              damage = 2 * attack;
              timeCriticalAttackOne = Date.now();
            } else return
          } else if (playerTwoCriticalHitCombination) {
            if ((timeCriticalAttackTwo === undefined) || ((Date.now() - timeCriticalAttackTwo) >= intervalCriticalAttack)) {
              attacker = secondFighter;
              let { attack } = secondFighter;
              healthDefender = healthFirst;
              fighterIndicator = leftFighterIndicator;
              damage = 2 * attack;
              timeCriticalAttackTwo = Date.now();
            } else return
          }
        }
        healthDefender.current -= damage;
        console.log('damage: ', damage);
        if (healthDefender.current <= 0) {
          fighterIndicator.style.width = `0%`;
          resolve(attacker)
          return;
        }
        let width = `${100 * healthDefender.current / healthDefender.full}`
        let color = '#ebd759';
        if (width < 20) color = 'red'
        fighterIndicator.style.cssText = `width:${width}%; background-color: ${color};`;

        damage = 0;
      } else {
        return
      }
    });

    document.addEventListener('keyup', function (event) {
      pressedFighter.delete(event.code);
      console.log('delite', pressedFighter);
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let damage = 0;
  if (defender === false) {
    damage = getHitPower(attacker);
  } else {
    damage = getHitPower(attacker) - getBlockPower(defender);
    if (damage < 0) {
      damage = 0;
    }
  }
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const power = attack * randomNumber();
  console.log('getHitPower', power);
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const power = defense * randomNumber();
  console.log('getBlockPower', power);
  return power;
}

//random number from 1 to 2
function randomNumber() {
  let number = Math.random() + 1;
  console.log('random', number)
  return number;
}

function criticalHitCombination(playerCriticalHitCombination, pressedFighter) {
  for (let code of playerCriticalHitCombination) {
    if (!pressedFighter.has(code)) return false;
  }
  return true;
}

