import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  try {
    const { name, health, attack, defense} = fighter;

    const imageElement = createFighterImage(fighter);

    const nameElement = createElement({ tagName: 'div', className: 'name' });
    nameElement.innerText = name;

    const healthElement = createElement({ tagName: 'div', className: 'health ability' });
    healthElement.innerText = `health: ${health}`;

    const attackElement = createElement({ tagName: 'div', className: 'attack ability' });
    attackElement.innerText = `attack: ${attack}`;

    const defenseElement = createElement({ tagName: 'div', className: 'defense ability' });
    defenseElement.innerText = `defense: ${defense}`;

    fighterElement.append(imageElement);
    fighterElement.append(nameElement);
    fighterElement.append(healthElement);
    fighterElement.append(attackElement);
    fighterElement.append(defenseElement);

  } catch (error) {
    
    const message = createElement({
      tagName: 'div',
      className: 'fighter-preview___right__message',
    });
    message.innerHTML = "Choose the opponent!";
    fighterElement.append(message);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
