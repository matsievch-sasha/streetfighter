import { showModal } from "./modal";
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function 
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const wrapperWinner = createElement({ tagName: 'div', className: 'wrapper-winner' });
  const textWinner = createElement({ tagName: 'div', className: 'text-winner', attributes:{ title: "WINNER!" } });
  textWinner.innerText = 'WINNER!';
  wrapperWinner.append(textWinner);
  bodyElement.append(wrapperWinner);
  const imageElement = createImage(fighter);
  bodyElement.append(imageElement);
  showModal({ title: fighter.name, bodyElement })
}

function createImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'winner___fighter-image',
    attributes
  });
  return imgElement;
}